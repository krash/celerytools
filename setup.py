from setuptools import setup, find_packages

setup(
    name='celerytools',
    version='0.1.2',
    description='Tools for the Celery',
    author='Leonid Krashenko',
    author_email='leonid.krashenko@yandex.ru',
    license='MIT',
    packages=find_packages('.', exclude=['test*']),
    install_requires=[
        'psutil==5.3.0',
        'pytest'
    ]
)
