Celerytools
===========

License
-------

MIT.

Tests
-----

To run tests use:

```bash
python -m pytest -s
```
