import subprocess
import os
# import sys

from time import sleep

from celery.platforms import signals
from celery.signals import worker_process_shutdown, worker_shutdown

from ..celery_app import app
# from celery.contrib import rdb


@app.task
def starter():
    stub_app = os.path.dirname(__file__) + '/stub.py'
    print(stub_app)
    proc = subprocess.Popen([stub_app])

    def term_handler(signal, frame):
        print("___SIGTERM, handling!")
        if proc is not None:
            proc.terminate()

    signals['TERM'] = term_handler

    while True:
        print("ping...")
        retcode = proc.poll()
        if retcode is None:
            pass
        else:
            if retcode != 0:
                raise Exception("stub error")
            else:
                break
        sleep(1)


@worker_process_shutdown.connect
def process_shutdown(signal, sender, pid, exitcode):
    print("___WORKER PROCESS %s SHUTDOWN WITH %s" % (pid, exitcode))


@worker_shutdown.connect
def shutdown(signal, sender):
    # from celery.contrib import rdb; rdb.set_trace()
    print("___WORKER SHUTDOWN")
