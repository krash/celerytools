# -*- coding: utf-8 -*-
from __future__ import absolute_import

import os
import subprocess
from celery.utils.log import get_task_logger

from ..celery_app import app
from .base import AbortableSubprocessTask

STUB_APP_PATH = os.path.dirname(__file__) + '/stub.py'

logger = get_task_logger(__name__)


@app.task(bind=True, base=AbortableSubprocessTask, logger=logger)
def progress_task(self, abort_after=None):
    if not abort_after:
        abort_after = 5

    STUB_COMMAND = "{stub} --progress {n}".format(
        stub=STUB_APP_PATH, n=abort_after
    )

    def on_progress(proc, out):
        logger.info("{proc} out:{out}".format(proc=proc, out=out))

    self.run_command(STUB_COMMAND, on_poll=on_progress, stdout=subprocess.PIPE)

    meta = {}
    return meta
