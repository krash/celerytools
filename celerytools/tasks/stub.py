#!/usr/bin/env python

import sys

from time import sleep
from optparse import OptionParser


def stub():
    while(True):
        sleep(1)


def progress(sec):
    slept = 0
    while(slept < sec):
        sleep(1)
        slept += 1
        print("{0}%%".format(slept*100/sec))
        sys.stdout.flush()


def main():
    usage = "%prog [options]"
    parser = OptionParser(usage)
    parser.add_option('-s', "--sleep", type='int', dest='sleep', default=None)
    parser.add_option('-p', "--progress", type='int', dest='progress', default=None)

    (options, args) = parser.parse_args()
    if options.sleep is not None:
        sleep(options.sleep)
        exit(0)

    if options.progress:
        progress(options.progress)
    else:
        stub()

    exit(0)


if __name__ == '__main__':
    main()
