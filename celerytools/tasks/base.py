# -*- coding: utf-8 -*-
from __future__ import absolute_import

import subprocess
import shlex
import os
from time import sleep

from celery.platforms import signals
from celery.contrib.abortable import AbortableTask


class AbortableSubprocessTask(AbortableTask):
    _procs = []
    _initialized = False
    _cleaned = False
    _handlers = {}

    logger = None

    def _app_name(self, cmd):
        cmd_args = shlex.split(cmd)
        return os.path.basename(cmd_args[0])

    def start_process(self, cmd, on_start=None, *args, **kwargs):
        self._cleaned = False
        if not self._initialized:
            self._setup_signal_handlers()

        app_name = self._app_name(cmd)
        self.logger.info("Starting process {}".format(app_name))

        cmd_args = shlex.split(cmd)
        proc = subprocess.Popen(cmd_args, *args, **kwargs)

        if on_start:
            on_start(proc)

        self._procs.append(proc)
        return proc

    def run_command(self, cmd, on_start=None, on_poll=None, *args, **kwargs):
        app_name = self._app_name(cmd)

        proc = self.start_process(cmd, on_start, *args, **kwargs)
        while True:
            if self.is_aborted():
                self.logger.warning("task aborted")
                self.cleanup()
                break

            # self.logger.info("ping... {}".format(app_name))
            retcode = proc.poll()
            if retcode is None:
                if on_poll:
                    line = proc.stdout.readline()
                    on_poll(proc, line)
                else:
                    pass
            else:
                if retcode < 0:
                    self.logger.warning("{} terminated".format(app_name))
                    break
                elif retcode > 0:
                    self.logger.warning("{} died unexpectedly with code %s"
                                        .format(app_name, retcode))
                    break
                else:
                    self.logger.info("{} finished successfully"
                                     .format(app_name))
                    break  # everything OK, just stop polling
            sleep(1)
        return proc

    def cleanup(self):
        # TODO check if process is already terminated
        if self._cleaned:
            self.logger.warning("Already clean")
            return

        self.logger.info("Cleaning up...")
        for proc in self._procs:
            if proc is not None:
                self.logger.warning("Terminating %s" % proc)
                proc.terminate()

        for proc in self._procs:
            if proc is not None:
                self.logger.info("Waiting for %s to terminate" % proc)
                proc.wait()

        self._cleaned = True
        self.logger.info("Clean up finished")

    def _setup_signal_handlers(self):
        # TODO signals in billiard/common.py
        # TODO WARNING: this is overriding 'billiard' shutdown defaults!
        self._handlers['TERM'] = signals['TERM']
        signals['TERM'] = self._term_handler
        self._initialized = True

    def _term_handler(self, signal, frame):
        self.cleanup()
        self._handlers['TERM'](signal, frame)
