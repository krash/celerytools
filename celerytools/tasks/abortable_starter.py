# -*- coding: utf-8 -*-
from __future__ import absolute_import

import os
import time

from celery.utils.log import get_task_logger

from ..celery_app import app

from .base import AbortableSubprocessTask


logger = get_task_logger(__name__)

STUB_APP_PATH = os.path.dirname(__file__) + '/stub.py'


@app.task(bind=True, base=AbortableSubprocessTask, logger=logger)
def abortable_starter(self, abort_after=None):
    # TODO on abort handler
    # TODO on started handler
    meta = {'procs': []}

    def on_started(proc):
        meta['procs'].append(proc.pid)
        self.update_state(state='STUBREADY', meta=meta)

    # TODO on progress handler
    if not abort_after:
        self.run_command(STUB_APP_PATH, on_start=on_started)
    else:
        self.run_command("{stub} --sleep {n}".format(
            stub=STUB_APP_PATH, n=abort_after), on_start=on_started)

    return meta


@app.task(bind=True, base=AbortableSubprocessTask, logger=logger)
def task_with_exception(self):
    '''Task with exception cleanup handler'''
    meta = {'procs': []}

    try:
        proc = self.start_process(STUB_APP_PATH)

        meta['procs'].append(proc.pid)
        self.update_state(state='STUBREADY', meta=meta)

        time.sleep(1)
        raise Exception("something happened")
    except Exception:
        logger.warning("got an exception, handling...")
        self.cleanup()
        raise


@app.task(bind=True, base=AbortableSubprocessTask, logger=logger)
def parallel_processes_task(self):
    '''Runs 2 subprocesses in parallel'''
    try:
        self.start_process(STUB_APP_PATH)
        self.start_process(STUB_APP_PATH)
    except Exception:
        logger.warning("got an exception, handling...")
        self.cleanup()


@app.task(bind=True, base=AbortableSubprocessTask, logger=logger)
def sequential_processes_task(self, abort_after):
    meta = {'procs': []}

    '''Runs 2 subprocesses sequentially'''
    try:
        stub1_proc = self.run_command("{stub} --sleep {n}".format(
            stub=STUB_APP_PATH, n=abort_after))
        meta['procs'].append(stub1_proc.pid)

        stub2_proc = self.run_command("{stub} --sleep {n}".format(
            stub=STUB_APP_PATH, n=abort_after))
        meta['procs'].append(stub2_proc.pid)
    except Exception:
        logger.warning("got an exception, handling...")
        self.cleanup()
    finally:
        return meta
