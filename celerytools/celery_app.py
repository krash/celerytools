from celery import Celery


class Config:
    BROKER_URL = 'redis://localhost:6379/5'
    BROKER_HEARTBEAT = False
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/5'
    CELERY_TRACK_STARTED = True
    CELERY_REDIRECT_STDOUTS_LEVEL = 'DEBUG'
    CELERY_TIMEZONE = 'UTC'
    CELERY_IMPORTS = ('celerytools.tasks',)
    CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']


app = Celery('celerytools')
app.config_from_object(Config)
