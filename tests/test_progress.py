import unittest

from celerytools.tasks import progress_task
from .utils import run_celery


class TestProgress(unittest.TestCase):

    def test_progress(self):
        try:
            celery_proc = run_celery()
            result = progress_task.delay()

            meta = result.get()

            print(meta)
        finally:
            celery_proc.terminate()
