import subprocess
import shlex


def run_celery():
    cmd = "celery -A celerytools.celery_app worker -l info"
    celery_proc = subprocess.Popen(shlex.split(cmd))
    return celery_proc
