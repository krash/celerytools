# import pytest; pytest.main()

import unittest
import psutil
import time

from celerytools.tasks import abortable_starter, task_with_exception, \
    sequential_processes_task
from .utils import run_celery


def terminated(process_names):
    for proc in psutil.process_iter():
        if proc.name in process_names:
            return False
    return True


def stub_ready(result, timeout=5):
    '''Wait for the stub subprocess in task is started'''
    seconds = 0
    while True and seconds < timeout:
        if result.state == 'STUBREADY':
            return True
        time.sleep(1)
        seconds = seconds + 1
    return False


class TestAbortable(unittest.TestCase):

    # TODO test manual abort 2 tasks

    def test_manual_abort(self):
        '''
        Test manual task abort.
        Test should:
        - start the stub subprocess in abortable task
        - abort that task
        - stop all celery workers
        - check nothing is running (including the stub)
        '''
        celery_proc = run_celery()

        result = abortable_starter.delay(abort_after=2)
        assert stub_ready(result)

        parent = psutil.Process(celery_proc.pid)
        children_pids = [p.pid for p in parent.children(recursive=True)]
        assert len(children_pids) == 9

        result.abort()

        # terminate celery
        celery_proc.terminate()
        celery_proc.wait()

        # assert celery and tasks are terminated
        for pid in children_pids:
            assert not psutil.pid_exists(pid)
        assert terminated(['celery'])

    def test_abortable_task_completion(self):
        '''
        Test task that completes execution after 2 seconds.
        Test should:
        - start the stub subprocess in abortable task
        - wait for the task to complete
        - check result was successful
        - stop all celery workers
        - check nothing is running (including the stup)
        '''
        celery_proc = run_celery()

        result = abortable_starter.delay(abort_after=2)

        assert stub_ready(result)

        parent = psutil.Process(celery_proc.pid)
        children_pids = [p.pid for p in parent.children(recursive=True)]

        result.get()
        assert result.successful()

        # terminate celery
        celery_proc.terminate()
        celery_proc.wait()

        # assert celery and tasks are terminated
        for pid in children_pids:
            assert not psutil.pid_exists(pid)
        assert terminated(['celery'])

    def test_task_sigterm(self):
        '''
        Test task that gets a TERM signal
        Test should:
        - start the stub subprocess in abortable task
        - terminate the stub worker
        - terminate celery
        - check nothing is running (including the stub subprocess)
        '''
        celery_proc = run_celery()

        result = abortable_starter.delay()

        assert stub_ready(result)
        meta = result.info
        stub_pid = meta['procs'][0]
        stub_proc = psutil.Process(stub_pid)
        stub_worker_proc = stub_proc.parent()

        parent = psutil.Process(celery_proc.pid)
        children_pids = [p.pid for p in parent.children(recursive=True)]

        # terminate stub woker
        assert psutil.pid_exists(stub_pid)
        stub_worker_proc.terminate()
        stub_worker_proc.wait()

        # terminate celery
        celery_proc.terminate()
        celery_proc.wait()

        # assert celery and tasks are terminated
        for pid in children_pids:
            assert not psutil.pid_exists(pid)
        assert terminated(['celery'])

    def test_task_exception(self):
        '''
        Test the task that raises an Exception when the stub is running.
        Test should:
        - start the stub subprocess in abortable task
        - wait for the task to raise an exception
        - check there is no stub suprocess running
        - then stop celery
        - check nothing is running
        '''
        celery_proc = run_celery()

        result = task_with_exception.delay()

        assert stub_ready(result)
        meta = result.info
        stub_pid = meta['procs'][0]

        parent = psutil.Process(celery_proc.pid)
        children_pids = [p.pid for p in parent.children(recursive=True)]

        # wait for the task to raise an exception
        try:
            assert psutil.pid_exists(stub_pid)
            result.get()  # should raise here
        except Exception:
            assert not psutil.pid_exists(stub_pid)

        # terminate celery
        celery_proc.terminate()
        celery_proc.wait()

        # assert celery and tasks are terminated
        for pid in children_pids:
            assert not psutil.pid_exists(pid)
        assert terminated(['celery'])

    def test_simultaneous_sigterm(self):
        '''
        Test two tasks running simultaneously.
        Test should:
        - start 2 tasks (with stub subprocesses)
        - terminate stub workers
        - terminate celery
        - check nothing is running (including stubs)
        This test is important to check if each of the tasks defined it's own
        TERM signal handler.
        '''
        celery_proc = run_celery()

        # run stub 1 and get meta info
        result1 = abortable_starter.delay()
        assert stub_ready(result1)
        meta = result1.info
        stub_pid1 = meta['procs'][0]
        stub_proc1 = psutil.Process(stub_pid1)
        stub_worker_proc1 = stub_proc1.parent()

        # run stub 2 and get meta info
        result2 = abortable_starter.delay()
        assert stub_ready(result2)
        meta = result2.info
        stub_pid2 = meta['procs'][0]
        stub_proc2 = psutil.Process(stub_pid2)
        stub_worker_proc2 = stub_proc2.parent()

        # get all celery pids (including stubs)
        parent = psutil.Process(celery_proc.pid)
        children_pids = [p.pid for p in parent.children(recursive=True)]

        # terminate stub wokers
        assert psutil.pid_exists(stub_pid1)
        assert psutil.pid_exists(stub_pid2)
        stub_worker_proc1.terminate()
        stub_worker_proc1.wait()
        stub_worker_proc2.terminate()
        stub_worker_proc2.wait()

        # terminate celery
        celery_proc.terminate()
        celery_proc.wait()

        # assert celery and tasks are terminated
        for pid in children_pids:
            assert not psutil.pid_exists(pid)
        assert terminated(['celery'])

    def test_task_command_sequence(self):
        '''
        Test the execution of the command sequence.
        Test should:
        - run task with command sequence, wait for task success.
        - stop celery;
        - check nothing is running.
        '''
        celery_proc = run_celery()

        result = sequential_processes_task.delay(abort_after=2)

        # wait for success
        result.get()
        assert result.successful()

        # check stubs not running
        meta = result.info
        assert len(meta['procs']) == 2
        assert not psutil.pid_exists(meta['procs'][0])
        assert not psutil.pid_exists(meta['procs'][1])

        # get workers pids
        parent = psutil.Process(celery_proc.pid)
        children_pids = [p.pid for p in parent.children(recursive=True)]

        # terminate celery
        celery_proc.terminate()
        celery_proc.wait()

        # assert celery and tasks are terminated
        for pid in children_pids:
            assert not psutil.pid_exists(pid)
        assert terminated(['celery'])
